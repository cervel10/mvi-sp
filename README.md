# Biosynthetic Gene Clusters (BGC) classification
Tento projekt slouží jako rozšíření existujícího nástroje DeepBGC (Hannigan et al., Nucleic Acids Research 2019) pod záštitou Discovery Informatics, MSD Czech Republic. Jeho obsah je dále zveřejněn v oficiálním github repozitáři (https://github.com/Merck/deepbgc) mým konzultantem Ing. Davidem Příhodou včetně reportu a finální prezentace projektu.

## Zadání
BGCs (biosynthetic gene clusters) jsou soubory ko-lokalizovaných genu, které produkují sekundární metabolity s významnými antimikrobiálními vlastnostmi. Ve své práci vycházím z nástroje DeepBGC, který má za úkol na základě vstupní DNA sekvence detekovat (predikovat) BGCs a v dalším kroku tyto clustery klasifikovat do tříd (terpeny, polyketidy apod.). 

Ve své práci se zaměřuji na vylepšení této multi-label klasifikace. Mým cílem je:

- Navrhnout nový klasifikační model využívající deep learning
- Porovnat ho se stávajícím modelem (Random Forest) a dalšími sklearn modely, které podporují multi-label klasifikaci.
- Vyzkoušet různé architektury neuronové sítě a zkoumat vliv změny parametrů na performance
- Vyzkoušet dva různé způsoby předzpracování dat a sledovat jejich vliv na úspěšnost klasifikace


## Pokyny pro spuštění
Pro spuštění hlavního jupyter notebooku je nutné nainstalovat DeepBGC (pozn.: Používám pro preprocessing vstupních dat). Stručné pokyny jsou obsaženy v hlavičce jupyter notebooku (BGC_classification.ipynb), v případě neúspěchu následujte pokyny pro instalaci v oficiálním github repozitáři, případně použijte environment.yml soubor ve složce data pro vytvoření conda prostředí (pozn.: veškeré dependencies byly testovány pro MacOS a Windows 11, je možné, že se vyskytnou nějaká úskalí s Linuxem).  

